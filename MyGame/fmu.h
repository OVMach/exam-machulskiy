//---------------------------------------------------------------------------

#ifndef fmuH
#define fmuH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include <Vcl.ExtCtrls.hpp>
#include <Vcl.Imaging.jpeg.hpp>
//---------------------------------------------------------------------------
class TfmMenu : public TForm
{
__published:	// IDE-managed Components
    TButton *buExit;
    TButton *buStart;
    void __fastcall buStartClick(TObject *Sender);
private:	// User declarations
public:		// User declarations
    __fastcall TfmMenu(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TfmMenu *fmMenu;
//---------------------------------------------------------------------------
#endif
