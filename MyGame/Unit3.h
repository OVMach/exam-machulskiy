//---------------------------------------------------------------------------

#ifndef Unit3H
#define Unit3H
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ExtCtrls.hpp>
#include <Graphics.hpp>
#include <Menus.hpp>
#include <jpeg.hpp>
#include <ImgList.hpp>
#include <System.ImageList.hpp>
#include <Vcl.Imaging.pngimage.hpp>
#include <Vcl.Imaging.GIFImg.hpp>
#include <Vcl.ComCtrls.hpp>
#include <Vcl.ToolWin.hpp>
//---------------------------------------------------------------------------
class TForm3 : public TForm
{
__published:	// IDE-managed Components
	TImage *Image1;
	TImage *Image2;
	TLabel *Label1;
    TLabel *lbAll;
	TImage *Image3;
	TMainMenu *MainMenu1;
	TMenuItem *df1;
	TMenuItem *Rebootthegame1;
	TTimer *Timer1;
	TTimer *Timer2;
	TImage *Image4;
	TImageList *ImageList1;
    TLabel *Label3;
    TLabel *lbSpeed;
    TLabel *Label5;
    TLabel *lbTimeup;
    TGridPanel *GridPanel1;
    TMenuItem *N1;
	void __fastcall KDown(TObject *Sender, WORD &Key, TShiftState Shift);
	void __fastcall Rebootthegame1Click(TObject *Sender);
	void __fastcall TickEvent(TObject *Sender);
    void __fastcall EventTimer(TObject *Sender);
    void __fastcall Button2Click(TObject *Sender);
    void __fastcall N1Click(TObject *Sender);
private:	// User declarations
public:		// User declarations
	__fastcall TForm3(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TForm3 *Form3;
//---------------------------------------------------------------------------
#endif
