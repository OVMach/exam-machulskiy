//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "Unit3.h"



//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TForm3 *Form3;
unsigned short frag_counter;
signed short speed = 10;
int global_time = 0;
static bool cant_cont = true;
static int where = 0;
static int timer = 10;
//---------------------------------------------------------------------------

struct startposition
{
	int top;
	int left;
} fig1,fig2;

#define Randomize(min,max)((rand()%(int)(((max)+1)-(min)))+(min))

enum Pos{DOWN_POS,LEFT_POS,UP_POS,RIGHT_POS};

__fastcall TForm3::TForm3(TComponent* Owner)
	: TForm(Owner)
{
	Timer1->Interval = speed;
	srand(time(NULL));
	Image2->Visible = true;
	fig1.top = Image2->Top;
	fig1.left = Image2->Left;
	fig2.top = Image1->Top;
	fig2.left = Image1->Left;
	ImageList1->GetBitmap(UP_POS, Image1->Picture->Bitmap);
}

enum HowToGo{UP = VK_UP,DOWN = VK_DOWN,LEFT = VK_LEFT,RIGHT = VK_RIGHT};

void __fastcall TForm3::KDown(TObject *Sender, WORD &Key, TShiftState Shift)
{
	Timer1->Enabled = false;
	if(!cant_cont) return;
	switch(Key)
	{
		case VK_UP :
			where = UP;
			do
			{
				if(!cant_cont) return;
				if(((Image1->Left > Image2->Left) && (Image1->Left < Image2->Left+Image2->Width)) && ((Image1->Top > Image2->Top) && (Image1->Top < Image2->Top + Image2->Height)))
				{
					Image2->Top = Randomize(30,Form3->ClientHeight-30);
					Image2->Left = Randomize(30,Form3->ClientWidth-30);
					frag_counter++;
					MessageBeep(MB_OK);
					lbAll->Caption = IntToStr(frag_counter);
				}
				if(Image1->Top <= 0)
				{
					MessageBeep(MB_ABORTRETRYIGNORE);
					lbAll->Caption = "���";
					Timer2->Enabled = false;
					Timer1->Enabled = false;
					Image3->Top = Image1->Top;
					Image3->Left = Image1->Left;
					Image3->Visible = true;
					cant_cont = false;
                    Image1->Visible = false;
					ShowMessage("�� ������� " + IntToStr(frag_counter) + " ������ �� " + IntToStr(global_time) + " ������");
                    global_time = 0;
				}
				Sleep(speed);
				ImageList1->GetBitmap(UP_POS, Image1->Picture->Bitmap);
				Image1->Top -= 1;
				Update();
			}while(GetAsyncKeyState(VK_UP));
			break;
		case VK_DOWN :
			where = DOWN;
			do
			{
				if(!cant_cont) return;
				if(((Image1->Left > Image2->Left) && (Image1->Left < Image2->Left+Image2->Width)) && ((Image1->Top > Image2->Top) && (Image1->Top < Image2->Top + Image2->Height)))
				{
					Image2->Top = Randomize(30,Form3->ClientHeight-30);
					Image2->Left = Randomize(30,Form3->ClientWidth-30);
					frag_counter++;
					MessageBeep(MB_OK);
					lbAll->Caption = IntToStr(frag_counter);
				}
				if(Image1->Top >= ClientHeight-20)
				{
					MessageBeep(MB_ABORTRETRYIGNORE);
					lbAll->Caption = "���";
					Timer2->Enabled = false;
					Timer1->Enabled = false;
					Image3->Top = Image1->Top;
					Image3->Left = Image1->Left;
					Image3->Visible = true;
					cant_cont = false;
                    Image1->Visible = false;
			        Image1->Visible = false;
					ShowMessage("�� ������� " + IntToStr(frag_counter) + " ������ �� " + IntToStr(global_time) + " ������");
                    global_time = 0;
				}
				ImageList1->GetBitmap(DOWN_POS, Image1->Picture->Bitmap);
				Image1->Top += 1;
				Sleep(speed);
				Update();
			}while(GetAsyncKeyState(VK_DOWN));
			break;
		case VK_LEFT :
			where = LEFT;
			do
			{
				if(!cant_cont) return;
				if(((Image1->Left > Image2->Left) && (Image1->Left < Image2->Left+Image2->Width)) && ((Image1->Top > Image2->Top) && (Image1->Top < Image2->Top + Image2->Height)))
				{
					Image2->Top = Randomize(30,Form3->ClientHeight-30);
					Image2->Left = Randomize(30,Form3->ClientWidth-30);
					frag_counter++;
					MessageBeep(MB_OK);
					lbAll->Caption = IntToStr(frag_counter);
				}
				if(Image1->Left <= 0)
				{
					MessageBeep(MB_ABORTRETRYIGNORE);
					lbAll->Caption = "���";
					Timer2->Enabled = false;
					Timer1->Enabled = false;
					Image3->Top = Image1->Top;
					Image3->Left = Image1->Left;
					Image3->Visible = true;
					cant_cont = false;
                    Image1->Visible = false;
					ShowMessage("�� ������� " + IntToStr(frag_counter) + " ������ �� " + IntToStr(global_time) + " ������");
                    global_time = 0;
				}
				ImageList1->GetBitmap(LEFT_POS, Image1->Picture->Bitmap);
				Image1->Left -= 1;
				Sleep(speed);
				Update();
			}while(GetAsyncKeyState(VK_LEFT));
			break;
		case VK_RIGHT :
			where = RIGHT;
			do
			{
				if(!cant_cont) return;
				if(((Image1->Left > Image2->Left) && (Image1->Left < Image2->Left+Image2->Width)) && ((Image1->Top > Image2->Top) && (Image1->Top < Image2->Top + Image2->Height)))
				{
					Image2->Top = Randomize(30,Form3->ClientHeight-30);
					Image2->Left = Randomize(30,Form3->ClientWidth-30);
					frag_counter++;
					MessageBeep(MB_OK);
					lbAll->Caption = IntToStr(frag_counter);
				}
				if(Image1->Left >= ClientWidth-20)
				{
					MessageBeep(MB_ABORTRETRYIGNORE);
					lbAll->Caption = "���";
					Timer2->Enabled = false;
					Timer1->Enabled = false;
					Image3->Top = Image1->Top;
					Image3->Left = Image1->Left;
					Image3->Visible = true;
					cant_cont = false;
                    Image1->Visible = false;
					ShowMessage("�� ������� " + IntToStr(frag_counter) + " ������ �� " + IntToStr(global_time) + " ������");
                    global_time = 0;
				}
				Image1->Left += 1;
				Sleep(speed);
				Update();
				ImageList1->GetBitmap(RIGHT_POS, Image1->Picture->Bitmap);
			}while(GetAsyncKeyState(VK_RIGHT));
			break;
	}
    Timer1->Enabled = !cant_cont ? false : true;
}
//---------------------------------------------------------------------------

void __fastcall TForm3::Rebootthegame1Click(TObject *Sender)
{
	frag_counter = 0;
	cant_cont = true;
	speed = 15;
	timer = 0;
	Image3->Visible = false;
	Timer1->Enabled = false;
    Timer2->Enabled = true;
	Image1->Top = fig2.top;
	Image1->Left = fig2.left;
	Image2->Top = fig1.top;
	Image2->Left = fig2.left;
    Image1->Visible = true;
    global_time = 0;
}
//---------------------------------------------------------------------------

void __fastcall TForm3::TickEvent(TObject *Sender)
{
	if(!cant_cont) this->Timer1->Enabled = false;
	switch(where)
	{
		case UP:
				if(((Image1->Left > Image2->Left) && (Image1->Left < Image2->Left+Image2->Width)) && ((Image1->Top > Image2->Top) && (Image1->Top < Image2->Top + Image2->Height)))
				{
					Image2->Top = Randomize(30,Form3->ClientHeight-30);
					Image2->Left = Randomize(30,Form3->ClientWidth-30);
					frag_counter++;
					MessageBeep(MB_OK);
					lbAll->Caption = IntToStr(frag_counter);
				}
				if(Image1->Top <= 0)
				{
					MessageBeep(MB_ABORTRETRYIGNORE);
					lbAll->Caption = "���";
					Image3->Left = Image1->Left;
					Image3->Visible = true;
					Timer2->Enabled = false;
					cant_cont = false;
                    Image3->Top = Image1->Top;
                    Image1->Visible = false;
					ShowMessage("�� ������� " + IntToStr(frag_counter) + " ������ �� " + IntToStr(global_time) + " ������");
                    global_time = 0;
				}
				Image1->Top -= 1;
				Update();
			break;
		case DOWN:
				if(((Image1->Left > Image2->Left) && (Image1->Left < Image2->Left+Image2->Width)) && ((Image1->Top > Image2->Top) && (Image1->Top < Image2->Top + Image2->Height)))
				{
					Image2->Top = Randomize(30,Form3->ClientHeight-30);
					Image2->Left = Randomize(30,Form3->ClientWidth-30);
					frag_counter++;
					MessageBeep(MB_OK);
					lbAll->Caption = IntToStr(frag_counter);
				}
				if(Image1->Top >= ClientHeight-20)
				{
					MessageBeep(MB_ABORTRETRYIGNORE);
					lbAll->Caption = "���";
					Image3->Top = Image1->Top;
					Timer2->Enabled = false;
					Image3->Left = Image1->Left;
					Image3->Visible = true;
					cant_cont = false;
                    Image1->Visible = false;
					ShowMessage("�� ������� " + IntToStr(frag_counter) + " ������ �� " + IntToStr(global_time) + " ������");
                    global_time = 0;
				}
				Image1->Top += 1;
				Update();
			break;
		case RIGHT:
				if(((Image1->Left > Image2->Left) && (Image1->Left < Image2->Left+Image2->Width)) && ((Image1->Top > Image2->Top) && (Image1->Top < Image2->Top + Image2->Height)))
				{
					Image2->Top = Randomize(30,Form3->ClientHeight-30);
					Image2->Left = Randomize(30,Form3->ClientWidth-30);
					frag_counter++;
					MessageBeep(MB_OK);
					lbAll->Caption = IntToStr(frag_counter);
				}
				if(Image1->Left >= ClientWidth-20)
				{
					MessageBeep(MB_ABORTRETRYIGNORE);
					lbAll->Caption = "���";
					Image3->Top = Image1->Top;
					Timer2->Enabled = false;
					Image3->Left = Image1->Left;
					Image3->Visible = true;
					cant_cont = false;
                    Image1->Visible = false;
					ShowMessage("�� ������� " + IntToStr(frag_counter) + " ������ �� " + IntToStr(global_time) + " ������");
                    global_time = 0;
				}
				Image1->Left += 1;
				Update();
			break;
		case LEFT:
				if(((Image1->Left > Image2->Left) && (Image1->Left < Image2->Left+Image2->Width)) && ((Image1->Top > Image2->Top) && (Image1->Top < Image2->Top + Image2->Height)))
				{
					Image2->Top = Randomize(30,Form3->ClientHeight-30);
					Image2->Left = Randomize(30,Form3->ClientWidth-30);
					frag_counter++;
					MessageBeep(MB_OK);
					lbAll->Caption = IntToStr(frag_counter);
				}
				if(Image1->Left <= 0)
				{
					MessageBeep(MB_ABORTRETRYIGNORE);
				    lbAll->Caption = "���";
                    Image1->Visible = false;
					Image3->Top = Image1->Top;
					Image3->Left = Image1->Left;
					Image3->Visible = true;
					cant_cont = false;
                    Image1->Visible = false;
					ShowMessage("�� ������� " + IntToStr(frag_counter) + " ������ �� " + IntToStr(global_time) + " ������");
                    global_time = 0;
				}
				Image1->Left -= 1;
				Update();
			break;
	}
}
//---------------------------------------------------------------------------



//---------------------------------------------------------------------------
void __fastcall TForm3::EventTimer(TObject *Sender)
{
    global_time++;
	timer = timer == 0 ? 10 : timer - 1;
    if(speed != 1) lbTimeup->Caption = IntToStr(timer);
	if(timer == 0){
		if(speed == 1){
			lbSpeed->Caption = IntToStr(1);
			return;
		}else
		{
			speed--;
		}
        lbSpeed->Caption = IntToStr(speed);
	}
}
//---------------------------------------------------------------------------



void __fastcall TForm3::Button2Click(TObject *Sender)
{
    	frag_counter = 0;
	cant_cont = true;
	speed = 15;
	timer = 0;
	Image3->Visible = false;
	Timer1->Enabled = false;
    Timer2->Enabled = true;
	Image1->Top = fig2.top;
	Image1->Left = fig2.left;
	Image2->Top = fig1.top;
	Image2->Left = fig2.left;
    Image1->Visible = true;
    global_time = 0;
}
//---------------------------------------------------------------------------

void __fastcall TForm3::N1Click(TObject *Sender)
{
    Close();
}
//---------------------------------------------------------------------------

