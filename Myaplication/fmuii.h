//---------------------------------------------------------------------------

#ifndef fmuiiH
#define fmuiiH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.Layouts.hpp>
#include <FMX.Memo.hpp>
#include <FMX.Objects.hpp>
#include <FMX.ScrollBox.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.Types.hpp>
#include <FMX.Edit.hpp>
//---------------------------------------------------------------------------
class TForm2 : public TForm
{
__published:	// IDE-managed Components
    TButton *Button1;
    TImage *Image1;
    TImage *Image2;
    TToolBar *ToolBar1;
    TLabel *lbBull;
    TLabel *lbCows;
    TLabel *Label3;
    TLayout *Layout1;
    TLayout *Layout2;
    TLabel *Label4;
    TLayout *Layout3;
    TEdit *Edit1;
    void __fastcall Button1Click(TObject *Sender);
private: int a, b, ran[4], i, j, c[4], bull, cow;
             	// User declarations
public:		// User declarations
    __fastcall TForm2(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TForm2 *Form2;
//---------------------------------------------------------------------------
#endif
