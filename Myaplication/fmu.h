//---------------------------------------------------------------------------

#ifndef fmuH
#define fmuH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Layouts.hpp>
#include <FMX.Types.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.Objects.hpp>
#include <FMX.Edit.hpp>
#include <FMX.TabControl.hpp>
#include <FMX.Memo.hpp>
#include <FMX.ScrollBox.hpp>
//---------------------------------------------------------------------------
class Tfm : public TForm
{
__published:	// IDE-managed Components
    TTabControl *tm;
    TTabItem *tiMenu;
    TLayout *Layout1;
    TButton *buStart;
    TButton *buExit;
    TButton *Button3;
    TTabItem *tiPlay;
    TLabel *Label4;
    TLayout *Layout2;
    TImage *Image2;
    TLabel *lbBull;
    TImage *Image3;
    TLabel *lbCows;
    TLayout *Layout4;
    TLabel *Label3;
    TButton *Button1;
    TEdit *Edit1;
    TTabItem *tiGameEnd;
    TButton *Button2;
    TLabel *Label7;
    TImage *Image4;
    TLayout *Layout3;
    TLabel *lb1;
    TLabel *lb4;
    TLabel *lb3;
    TLabel *lb2;
    TLabel *Label8;
    TTabItem *tiRule;
    TButton *Button4;
    TLabel *Label1;
    TImage *Image5;
    TLabel *Label2;
    TImage *Image1;
    TLabel *Label5;
    TImage *Image6;
    TTabItem *tiWin;
    TImage *Image7;
    TLabel *Label6;
    TLabel *Label9;
    TLabel *Label10;
    TLayout *Layout5;
    TButton *Button5;
    TButton *Button6;
    TImage *Image8;
    TMemo *Memo1;
    void __fastcall AllMouseEnter(TObject *Sender);
    void __fastcall AllMouseLeave(TObject *Sender);
    void __fastcall buExitClick(TObject *Sender);
    void __fastcall buStartClick(TObject *Sender);
    void __fastcall Button1Click(TObject *Sender);
    void __fastcall Button2Click(TObject *Sender);
    void __fastcall FormCreate(TObject *Sender);
    void __fastcall Button3Click(TObject *Sender);
    void __fastcall Button5Click(TObject *Sender);
    void __fastcall Button6Click(TObject *Sender);
    void __fastcall Button4Click(TObject *Sender);
private: 	// User declarations
public:	int a, b, ran[4], i,j, c[4], bull, cow,x,s;	// User declarations
    __fastcall Tfm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE Tfm *fm;
//---------------------------------------------------------------------------
#endif
