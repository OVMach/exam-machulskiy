//---------------------------------------------------------------------------

#ifndef fmu1H
#define fmu1H
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.Objects.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.TabControl.hpp>
#include <FMX.Types.hpp>
//---------------------------------------------------------------------------
class TForm2 : public TForm
{
__published:	// IDE-managed Components
    TTabControl *TabControl1;
    TTabItem *tbStart;
    TButton *buAnsverA;
    TButton *buAnswerB;
    TButton *buAnswerC;
    TRectangle *Rectangle1;
    TImage *Image1;
    TLabel *Label1;
    TTabItem *TabItem2;
    TButton *Button1;
    TButton *Button2;
    TButton *Button3;
    TRectangle *Rectangle2;
    TLabel *Label2;
    TImage *Image2;
    TTabItem *TabItem1;
    TTabItem *TabItem3;
    TButton *Button4;
    TButton *Button5;
    TButton *Button6;
    TRectangle *Rectangle3;
    TLabel *Label3;
    TImage *Image3;
    TTabItem *TabItem4;
    TButton *Button7;
    TButton *Button8;
    TButton *Button9;
    TRectangle *Rectangle4;
    TLabel *Label4;
    TImage *Image4;
    TTabItem *TabItem5;
    TButton *Button10;
    TButton *Button11;
    TButton *Button12;
    TRectangle *Rectangle5;
    TLabel *Label5;
    TImage *Image5;
    TTabItem *TabItem6;
    TButton *Button13;
    TButton *Button14;
    TButton *Button15;
    TRectangle *Rectangle6;
    TLabel *Label6;
    TImage *Image6;
    TTabItem *TabItem7;
    TButton *Button16;
    TButton *Button17;
    TButton *Button18;
    TRectangle *Rectangle7;
    TLabel *Label7;
    TImage *Image7;
    TTabItem *TabItem8;
    TButton *Button19;
    TButton *Button20;
    TButton *Button21;
    TRectangle *Rectangle8;
    TLabel *Label8;
    TImage *Image8;
    TTabItem *TabItem9;
    TButton *Button22;
    TButton *Button23;
    TButton *Button24;
    TRectangle *Rectangle9;
    TLabel *Label9;
    TImage *Image9;
private:	// User declarations
public:		// User declarations
    __fastcall TForm2(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TForm2 *Form2;
//---------------------------------------------------------------------------
#endif
