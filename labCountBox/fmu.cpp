//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "fmu.h"
#include "unRandow.h"
#include "labCountBoxPCH1.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
Tfm *fm;
//---------------------------------------------------------------------------
__fastcall Tfm::Tfm(TComponent* Owner)
    : TForm(Owner)
{


}
void Tfm::DoContinue()
{
    for(int i=0;i<cMaxBox;i++) {
        ((TRectangle*)FListBox->Items[i])->Fill->Color = TAlphaColorRec::Lightgray;

    }
    FNumberCorrect =RandomRange(cMinPossible,cMaxPossible);
    int *x = RandomArrayUnique(cMaxBox,FNumberCorrect);
    for(int i=0;i<FNumberCorrect;i++) {
        ((TRectangle*)FListBox->Items[x[i]])->Fill->Color = TAlphaColorRec::Red;

    }
    int xAnswerStart = FNumberCorrect-Random(cMaxAnswer-1);
    if (xAnswerStart<cMinPossible)
        xAnswerStart=cMinPossible;

    for (int i =0;i<cMaxAnswer;i++) {
        ((TButton*)FListAnswer->Items[i])->Text= IntToStr(xAnswerStart + i);
    }

}
void Tfm::DoAnswer(int aValue)
{
    (aValue == FNumberCorrect)? FCountCorrect++ : FCountWrong++;
    if(FCountWrong >5 ){
        lbResult->Items->Add("������� �����������: "+IntToStr(FCountWrong));
        lbResult->Items->Add("������� ���������: "+IntToStr(FCountCorrect) );
        tm->GotoVisibleTab(tbResult->Index);
    }
    DoContinue();
}
void Tfm::DoFinish()
{
    tmPlay->Enabled=false;
    lbResult->Items->Add("������� �����������: "+IntToStr(FCountWrong));
    lbResult->Items->Add("������� ���������: "+IntToStr(FCountCorrect) );
    tm->GotoVisibleTab(tbResult->Index);

}

//---------------------------------------------------------------------------
void __fastcall Tfm::Button9Click(TObject *Sender)
{
    Close();
}
//---------------------------------------------------------------------------



void __fastcall Tfm::buAnswAllClick(TObject *Sender)
{
    DoAnswer(StrToInt(((TButton*)Sender)->Text));
}
//---------------------------------------------------------------------------

void __fastcall Tfm::FormCreate(TObject *Sender)
{
    FListBox= new TList;
    for (int i=1;i <= cMaxBox; i++) {
        FListBox->Add(this->FindComponent("Rectangle"+IntToStr(i)));
        FListAnswer= new TList;
        FListAnswer->Add(buAnsw1);
        FListAnswer->Add(buAnsw2);
        FListAnswer->Add(buAnsw3);
        FListAnswer->Add(buAnsw4);
        FListAnswer->Add(buAnsw5);
        FListAnswer->Add(buAnsw6);
    }

}
//---------------------------------------------------------------------------

void __fastcall Tfm::FormDestroy(TObject *Sender)
{
    delete FListBox;
    delete FListAnswer;
}
void Tfm::DoReset()
{
    FCountCorrect=0;
    FCountWrong=0;
    FTimeValue = Time().Val + (double)30/(24*60*60);
    tmPlay->Enabled = true;
    lbResult->Clear();
    DoContinue();
}

//---------------------------------------------------------------------------

void __fastcall Tfm::Timer1Timer(TObject *Sender)
{

//---------------------------------------------------------------------------
 }
void __fastcall Tfm::Button1Click(TObject *Sender)
{
    tm->GotoVisibleTab(tiMenu->Index);
}
//---------------------------------------------------------------------------

void __fastcall Tfm::tmPlayTimer(TObject *Sender)
{
      double x = FTimeValue - Time().Val;
    laTime->Text = FormatDateTime("nn:ss",x);
    if(x<=0)
       DoFinish();
}

//---------------------------------------------------------------------------

void __fastcall Tfm::buStartClick(TObject *Sender)
{
	tm->GotoVisibleTab(tiPlay->Index);
     DoReset();
}
//---------------------------------------------------------------------------

void __fastcall Tfm::Button2Click(TObject *Sender)
{
    DoReset();

}
//---------------------------------------------------------------------------


void __fastcall Tfm::Button3Click(TObject *Sender)
{
	  tm->GotoVisibleTab(tiMenu->Index);
}
//---------------------------------------------------------------------------

void __fastcall Tfm::allEnter(TObject *Sender)
{
    TButton *x=((TButton*)Sender);
    x->Margins->Rect=TRect(0,0,0,0);
    x->TextSettings->Font->Size +=10;
    x->TextSettings->Font->Style=x->TextSettings->Font->Style <<TFontStyle::fsBold;
}
//---------------------------------------------------------------------------

void __fastcall Tfm::allLeave(TObject *Sender)
{
    TButton *x=((TButton*)Sender);
    x->Margins->Rect=TRect(5,5,5,5);
    x->TextSettings->Font->Size -=10;
    x->TextSettings->Font->Style=x->TextSettings->Font->Style >>TFontStyle::fsBold;

}
//---------------------------------------------------------------------------

void __fastcall Tfm::Button10Click(TObject *Sender)
{
    tm->GotoVisibleTab(tbAbout->Index);
}
//---------------------------------------------------------------------------

