//---------------------------------------------------------------------------

#ifndef fmuH
#define fmuH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.Layouts.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.TabControl.hpp>
#include <FMX.Types.hpp>
#include <FMX.Objects.hpp>
#include <FMX.ListBox.hpp>
#include <FMX.Media.hpp>
//---------------------------------------------------------------------------
class Tfm : public TForm
{
__published:	// IDE-managed Components
    TTabControl *tm;
    TTabItem *tiMenu;
    TTabItem *tiPlay;
    TLayout *Layout1;
    TButton *Button1;
    TGridPanelLayout *GridPanelLayout1;
    TButton *buAnsw1;
    TButton *buAnsw2;
    TButton *buAnsw3;
    TButton *buAnsw4;
    TButton *buAnsw5;
    TButton *buAnsw6;
    TLayout *Layout2;
    TGridPanelLayout *GridPanelLayout2;
    TRectangle *Rectangle1;
    TRectangle *Rectangle2;
    TRectangle *Rectangle3;
    TRectangle *Rectangle4;
    TRectangle *Rectangle5;
    TRectangle *Rectangle6;
    TRectangle *Rectangle7;
    TRectangle *Rectangle8;
    TRectangle *Rectangle9;
    TRectangle *Rectangle10;
    TRectangle *Rectangle11;
    TRectangle *Rectangle12;
    TRectangle *Rectangle13;
    TRectangle *Rectangle14;
    TRectangle *Rectangle15;
    TRectangle *Rectangle16;
    TRectangle *Rectangle17;
    TRectangle *Rectangle18;
    TRectangle *Rectangle19;
    TRectangle *Rectangle20;
    TRectangle *Rectangle21;
    TRectangle *Rectangle22;
    TRectangle *Rectangle23;
    TRectangle *Rectangle24;
    TRectangle *Rectangle25;
    TLayout *Layout3;
    TButton *buStart;
    TButton *Button9;
    TButton *Button10;
    TImage *Image1;
    TTimer *tmPlay;
    TLabel *laTime;
    TButton *Button2;
    TTabItem *tbResult;
    TListBox *lbResult;
    TLayout *Layout4;
    TButton *Button3;
    TImage *Image2;
    TLabel *Label1;
    TStyleBook *StyleBook1;
    TMediaPlayer *MediaPlayer1;
	TTabItem *tbAbout;
	TLayout *Layout5;
	TButton *Button4;
	TLabel *Label2;
	TLabel *Label3;
    void __fastcall Button9Click(TObject *Sender);
    void __fastcall buAnswAllClick(TObject *Sender);
    void __fastcall FormCreate(TObject *Sender);
    void __fastcall FormDestroy(TObject *Sender);
    void __fastcall Timer1Timer(TObject *Sender);
    void __fastcall Button1Click(TObject *Sender);
    void __fastcall tmPlayTimer(TObject *Sender);
    void __fastcall buStartClick(TObject *Sender);
    void __fastcall Button2Click(TObject *Sender);
    void __fastcall Button3Click(TObject *Sender);
    void __fastcall allEnter(TObject *Sender);
    void __fastcall allLeave(TObject *Sender);
	void __fastcall Button10Click(TObject *Sender);
private:  int FCountCorrect;
          int FCountWrong;
          int FNumberCorrect;
          double FTimeValue;
          TList *FListBox;
          TList *FListAnswer;
          void DoReset();
          void DoContinue();
          void DoAnswer(int aValue);
          void DoFinish();
          	// User declarations
public:		// User declarations
    __fastcall Tfm(TComponent* Owner);
};
          const int cMaxBox = 25;
          const int cMaxAnswer =6;
          const int cMinPossible =4;
          const int cMaxPossible =14;
//---------------------------------------------------------------------------
extern PACKAGE Tfm *fm;
//---------------------------------------------------------------------------
#endif
